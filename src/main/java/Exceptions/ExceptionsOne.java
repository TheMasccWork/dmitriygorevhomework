package main.java.Exceptions;

public class ExceptionsOne {
    public static void main(String[] args) {
        exceptionArray();
        exceptionTwo();

    }

    static void exceptionArray() {
        try {
            String[] array = new String[5];
            array[6] = "Ячейка";
            System.out.println(array[6]);
        } catch (Exception ex) {

        } finally {
            System.out.println("Выполняемый блок");
        }
        System.out.println("Программа завершена");
    }

    static void exceptionTwo() {
        try {
            int x = 0;
            int str = 10 / x;
            System.out.println(str);
        } catch (ArithmeticException e) {
        } finally {
            System.out.println("finali");
        }
    }
}
