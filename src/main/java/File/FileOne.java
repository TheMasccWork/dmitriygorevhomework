package main.java.File;

import java.io.File;

// Напишите программу на Java, чтобы получить список всех имен файлов / каталогов из заданных.
public class FileOne {

    public static class Exercise1 {
        public static void main(String a[])
        {
            File file = new File("src/main/java");
            String[] fileList = file.list();
            for(String name:fileList){
                System.out.println(name);
            }
        }
    }

}
