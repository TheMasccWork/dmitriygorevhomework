package main.java.File;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileFhree {
    public static void main(String[] args) throws IOException {
        double e = 2.7182818284590452353602875;
        System.out.println("Число E равно 2.7182818284590452353602875");
        int n = 9;
        System.out.println("Округляем до " + n + " знаков");

        String result = String.format("%." + n + "f", e);
        System.out.print("Записываем число " + result + " в файл output.txt");

        File file = new File("src/main/java/File/output.txt");
        file.createNewFile();
        Files.write(Paths.get("src//main/java/File/output.txt"), result.getBytes());

    }
}
