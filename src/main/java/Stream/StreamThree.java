package main.java.Stream;

//   У вас есть коллекция целых чисел.
//   Прибавьте ко всем элементам 1 и выведите количество элементов в коллекции.

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StreamThree {
    public static void main(String[] args) {

        List<Integer> collection2 = Arrays.asList(10, 6, 3, -7, 8, 25, 34, 17, 11, -24, 12, 9);
        System.out.println(collection2);

        List<Integer> evenNumber = collection2
                .stream().map(s -> s + 1)
                .collect(Collectors.toList());
        System.out.println(evenNumber);

        long quantity = evenNumber.stream().map(s -> s + 1).count();
        System.out.println("Число эллементов " + quantity);

    }
}