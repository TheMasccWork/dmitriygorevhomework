package main.java.Stream;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class Stream {

    public static void main(String[] args) {

        // Задача 1
        // У вас есть коллекция строк. Выведите ее на консоль.
        Collection<String> collection = Arrays.
                asList("Кот", "Собака", "Кролик", "Корова", "Гусь", "Коза", "Бык", "Курица");
        collection.forEach(x -> System.out.print(x + " "));

        // Задача 2
        // У вас есть коллекция целых чисел в которой 6 четных и столько же не четных чисел.
        // Превратите её в коллекцию четных чисел.

        System.out.println();
        Collection<Integer> collection2 = Arrays.asList(10, 6, 3, -7, 8, 25, 34, 17, 11, -24, 12, 9);
        System.out.println(collection2);

        List<Integer> evenNumber = collection2
                .stream().filter(n -> n % 2 == 0)
                .collect(Collectors.toList());
        System.out.println(evenNumber);
    }


}
