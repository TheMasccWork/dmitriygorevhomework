package main.java.LessonFive;

// Даны два слова. Для каждой буквы первого слова (в том числе для повторяющихся в этом слове букв)
// определить, входит ли она во второе слово.
// Например, если заданные слова информация и процессор, то для букв первого из них
// ответом должно быть: нет нет нет да да нет нет да нет нет.

import java.util.Arrays;

public class StringThree {
    public static void main(String[] args) {
        String a = "информация", b = "процессор";
        String[] arrayA = a.split("");
        String[] arrayB = b.split("");

        for (int i = 0; i < arrayA.length; i++)
            System.out.print(arrayA[i]);

        System.out.println();
        for (int i = 0; i < arrayB.length; i++)
            System.out.print(arrayB[i]);

        System.out.println();

        for (int i = 0; i < arrayA.length; i++) {

            boolean contains = Arrays.asList(arrayB).contains(arrayA[i]);

            System.out.print(contains + " ");
        }


    }
}
