package main.java.LessonFive;

public class LessonFiveExOne {
    public static void main(String[] args) {
        //   Даны два слова. Верно ли, что первое слово начинается на ту же букву,
        //   на которую заканчивается второе слово?

        String a = "dog";
        String b = "eagle";
        char c = a.charAt(0);
        char d = b.charAt(b.length()-1);
        System.out.print(c + " ");
        System.out.println(d);
        System.out.println(c == d);
    }
}
