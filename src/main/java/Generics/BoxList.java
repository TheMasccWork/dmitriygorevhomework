package main.java.Generics;

import java.util.LinkedList;
import java.util.List;

public class BoxList<T> {
    T value;
    List<T> list = new LinkedList<T>();

    public BoxList(T value) {
        this.value = value;
    }

    public BoxList(List<T> list) {
        this.list = list;

    }
}