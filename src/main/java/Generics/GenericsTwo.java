package main.java.Generics;


// Дана обобщенная (List<T>)  коллекция  и ещё одна переменная типа Т.
// Удалите все вхождения этой переменной  из коллекции.

public class GenericsTwo {
    public static void main(String[] args) {

        BoxList<String> box = new BoxList<>("Java");
        box.list.add("Java");
        box.list.add("Java");
        box.list.add("Java");
        box.list.add("Get");
        box.list.add("Hot");
        box.list.add("Java");
        box.list.add("Java");
        box.list.add("Home");
        System.out.print("Коллекция " + box.list);
        System.out.println();
        System.out.println("Значение переменной " + box.value);

        while (box.list.remove(box.value)) ;

        System.out.println("Измененная коллекция " + box.list);


    }
}