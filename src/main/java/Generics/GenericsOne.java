package main.java.Generics;

import static main.java.Generics.Method.isEqualTo;

public class GenericsOne {

// Напишите простую универсальную версию метода isEqualTo, которая сравнивает два его аргумента
// с методом equals и возвращает true, если они равны, и false в противном случае.
// Используйте этот универсальный метод в программе, которая вызывает isEqualTo с различными
// встроенными типами, такие как объект или целое число.
// Какой результат вы получите при попытке запустить эту программу?

    public static void main(String[] args) {

        Integer number1 = 50;
        int number2 = 50;
        String str1 = new String("50");
        String str2 = "50";
        System.out.println(isEqualTo(number1, number2));
        System.out.println(isEqualTo(str1, str2));
        System.out.println(isEqualTo(str1, number1));
        System.out.println(isEqualTo(str2, number2));
    }

}