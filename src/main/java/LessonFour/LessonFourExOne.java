package main.java.LessonFour;

import java.util.Scanner;

public class LessonFourExOne {
//    Мастям игральных карт присвоены порядковые номера:
//    1 — пики,  2 — трефы, 3 — бубны, 4 — червы.
//    Достоинству карт, старших десятки, присвоены номера:
//    11 — валет, 12 — дама, 13 — король, 14 — туз.
//    Даны два целых числа:
//    N — достоинство (6 ≤ N ≤ 14)
//    M — масть карты (1 ≤ M ≤ 4).
//    Вывести название соответствующей карты вида
//    «шестерка бубен», «дама червей», «туз треф» и т. п.

    public static void main(String[] args) {
        System.out.print("ВВеди достоинство карты от 6 до 14: ");
        Scanner scanner = new Scanner(System.in);
        int d = scanner.nextInt();
        System.out.print("ВВеди масть от 1 до 4: ");
        int mast = scanner.nextInt();

        String mast2 = " ";
        String d2 = " ";
        if (d < 6 || d > 14 || mast < 1 || mast > 4) {
            System.out.println("Неправильный ввод");
        } else {
            switch (mast) {
                case 1:
                    mast2 = "пик";
                    break;
                case 2:
                    mast2 = "треф";
                    break;
                case 3:
                    mast2 = "бубны";
                    break;
                case 4:
                    mast2 = "червы";
                    break;
            }

            switch (d) {
                case 6:
                    d2 = "Шестерка ";
                    break;
                case 7:
                    d2 = "Семерка ";
                    break;
                case 8:
                    d2 = "Восьмерка ";
                    break;
                case 9:
                    d2 = "Девятка ";
                    break;
                case 10:
                    d2 = "Десятка ";
                    break;
                case 11:
                    d2 = "Валет ";
                    break;
                case 12:
                    d2 = "Дама ";
                    break;
                case 13:
                    d2 = "Король ";
                    break;
                case 14:
                    d2 = "Туз ";
                    break;
            }
            System.out.println("Ответ " + d2 + mast2);
        }
    }
}
