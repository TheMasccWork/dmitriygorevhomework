package main.java.LessonFour.LessonFourExTwo;

public class Client {
    //    Клиенты имеют имя, фамилию, отчество, профессию,хобби. Умеют звонить,писать, говорить.
    String name;
    String surname;
    String patronymic;
    boolean profession;
    boolean hobby;

    void call() {
        System.out.println("Звонит");
    }

    void write() {
        System.out.println("Пишет");
    }

    void speak() {
        System.out.println("Говорит");
    }

    public Client(String name, String surname, String patronymic, boolean profession, boolean hobby) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.profession = profession;
        this.hobby = hobby;


    }
}
