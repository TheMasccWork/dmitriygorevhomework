package main.java.LessonFour.LessonFourExTwo;

public class Example {

    public static void main(String[] args) {


        Client ivanov = new Client("Сергей", "Иванов", "Петрович", true, true);
        Client petrov = new Client("Алексей", "Петров", "Иванович", false, false);
        Client sidorov = new Client("Юрий", "Сидоров", "Александрович", true, false);
        Client smirnov = new Client("Сергей", "Смитров", "Павлович", false, true);
        Client bokov = new Client("Геннадий", "Боков", "Евгеньевич", true, true);
        Client antonova = new Client("Светлана", "Антонова", "Сергеевна", true, false);

        Client[] clients = {ivanov, petrov, sidorov, smirnov, bokov, antonova};

        Employee gusev = new Employee("Николай", "Гусев", "Директор", "ул.Смирнова д.5", true);
        Employee volkov = new Employee("Петр", "Васильевич", "Юрист", "ул.Кузнецова д.46", true);
        Employee rojkov = new Employee("Илья", "Рожков", "Помощник", "ул.Радищева д.10", false);
        Employee romanov = new Employee("Игорь", "Романов", "Курьер", "ул.Куконковых д.132", true);

        Employee[] employees = {gusev, volkov, rojkov, romanov};

        Firma firma = new Firma("Юрист", "10-Августа д.30", employees, clients);
        System.out.println("В фирме " + firma.name + " " + firma.address);
        System.out.println(rojkov.name + " " + rojkov.surname + " " + rojkov.address + " Автомобиль " + rojkov.car);
        rojkov.work();
        System.out.println("С клиентом");
        System.out.println(sidorov.name + " " + sidorov.surname + " " + sidorov.patronymic);
        sidorov.speak();


    }



}

