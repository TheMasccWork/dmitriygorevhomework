package main.java.LessonFour.LessonFourExTwo;

public class Employee {
    //    Сотрудники имеют имя, фамилию, должность, адрес, машину и могут работать,
//    говорить, ходить и принимать заказ у клиентов.

    String name;
    String surname;
    String post;
    String address;
    boolean car;

    void work() {
        System.out.println("Работает");
    }

    void speak() {
        System.out.println("Говорит");
    }

    void go() {
        System.out.println("Ходит");
    }

    void accept() {
        System.out.println("Принимает заказ");
    }

    public Employee(String name, String surname, String post, String address, boolean car) {
        this.name = name;
        this.surname = surname;
        this.post = post;
        this.address = address;
        this.car = car;
    }
}
