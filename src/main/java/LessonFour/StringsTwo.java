package main.java.LessonFour;

import java.util.ArrayList;

public class StringsTwo {
    public static void main(String[] args) {
        text("У Пети было 12 яблок, 4 яблока отдал Васе и 2 яблока съел");
    }

    static void text(String text) {

        ArrayList<Integer> numbers = new ArrayList<Integer>();
        String[] textArr = text.split(" ");
        for (String a : textArr) {
            try {
                int i = Integer.valueOf(a);
                numbers.add(i);
                System.out.print(i + " ");
            } catch (NumberFormatException ex) {

            }
        }
    }
}
