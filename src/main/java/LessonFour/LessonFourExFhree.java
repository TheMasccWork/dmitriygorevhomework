package main.java.LessonFour;

public class LessonFourExFhree {
    //  Напишите программу, которая меняет местами элементы одномерного массива из String в обратном порядке.
    //  Не используйте дополнительный массив для хранения результатов.
    public static void main(String[] args) {
        String[] array = {"A", "B", "C", "D", "E", "F"};
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);
        }
        System.out.println();

        for (int i = 0; i < array.length/2; i++) {
            String temp = array[array.length - i - 1];
            array[array.length - i - 1] = array[i];
            array[i] = temp;
        }

        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i]);

        }
    }
}


