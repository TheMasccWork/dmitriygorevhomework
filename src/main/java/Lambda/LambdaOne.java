package main.java.Lambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LambdaOne {

    // Найти в массиве те элементы, значение которых меньше среднего арифметического,
    // взятого от всех элементов массива.

    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>(Arrays.asList(1, 20, 3, -4, 37, 0, 10, 62));
        Double averageValue = list.stream()
                .mapToDouble(value -> value)
                .average().orElse(0);

        System.out.println("Среднее значение коллекции " + averageValue);

        List<Integer> number = list.stream()
                .filter(n -> n < averageValue)
                .collect(Collectors.toList());
        System.out.println("Числа меньше среднего значения " + number);
    }
}