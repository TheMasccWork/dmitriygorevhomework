package main.java.Lambda;

//В массиве, содержащем положительные и отрицательные целые числа,
//вычислить сумму четных положительных элементов.

public class LambdaTwo {
    public static void main(String[] args) {
        int[] myArray = {2, 10, -23, 5, 4, 5, 43, -42, 35, 3, -34, 5, -97, 6, 22,};
        int j, sum = 0;
        Sum isEven = n -> (n % 2) == 0 && n >= 0;
        for (j = 0; j < myArray.length; j++) {

            if (isEven.checkEven(myArray[j])) {
                sum = sum + myArray[j];
            }
        }
        System.out.println(sum);
    }
}