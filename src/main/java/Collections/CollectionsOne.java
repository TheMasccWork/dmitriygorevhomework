package main.java.Collections;

import java.util.ArrayList;

public class CollectionsOne {
    //Определить сумму всех элементов коллекции.
    public static void main(String[] args) {

        ArrayList<Integer> lst = new ArrayList<>();
        lst.add(10);
        lst.add(20);
        lst.add(30);
        lst.add(40);
        lst.add(50);
        System.out.println(lst);

        int sum = 0;
        for (Integer integer : lst)
            sum = sum + integer;
        System.out.println("Сумма всех элементов коллекции равна " + sum);

    }
}