package main.java.Collections;

import java.util.ArrayList;
import java.util.List;

public class CollectionsTwo {
    //Найти элемент, наиболее близкий к среднему значению всех элементов коллекции

    public static void main(String[] args) {

        List<Double> lst = new ArrayList<>();
        lst.add(4.0);
        lst.add(50.0);
        lst.add(14.0);
        lst.add(6.0);
        lst.add(100.0);
        lst.add(-100.0);
        System.out.println(lst);

        double n = 0;
        for (int i = 0; i < lst.size(); i++) {
            n = n + lst.get(i);
        }
        double sr = n / lst.size();

        System.out.println("Среднее значение коллекции " + sr);

        double difference = Math.abs(lst.get(0) - sr);
        int index = 0;
        for (int i = 0; i < lst.size(); i++) {
            double distance = Math.abs(lst.get(i) - sr);
            if (distance < difference) {
                index = i;
                difference = distance;

            }
        }
        System.out.println("Ближайшее к среднему значению " + lst.get(index));

    }
}
