package main.java.Office;

public class Main {


    public static void main(String[] args) {
        Office office = new Office();

        Technics scanner1 = new Scanner(1, true);
        Technics combain1 = new Combain(2, true);
        Technics printer1 = new Printer(3, false);

        OfficeЕmployee ivanov = new OfficeЕmployee("Николай", "Иванов", "Сергеевич");
        OfficeЕmployee volkov = new OfficeЕmployee("Владимир", "Волков", "Александрович");
        OfficeЕmployee gusev = new OfficeЕmployee("Александр", "Гусев", "Георгиевич");
        OfficeЕmployee bokov = new OfficeЕmployee("Евгений", "Боков", "Романович");
        OfficeЕmployee zaicev = new OfficeЕmployee("Максим", "Зайцев", "Иванович");
        OfficeЕmployee orlov = new OfficeЕmployee("Роман", "Орлов", "Владимирович");

        Room room1 = new Room(101, scanner1);
        Room room2 = new Room(102, combain1);
        Room room3 = new Room(103, printer1);

        office.addRooms(room1);
        office.addRooms(room2);
        office.addRooms(room3);

        room1.addEmployee(ivanov);
        room1.addEmployee(volkov);
        room1.addEmployee(gusev);
        room2.addEmployee(bokov);
        room3.addEmployee(zaicev);
        room3.addEmployee(orlov);

        office.infoOffice();

    }
}




