package main.java.Office;

import java.util.ArrayList;
import java.util.List;

public class Room {

    private int number;
    private Technics technics;

    private List<OfficeЕmployee> employees = new ArrayList();

    public void addEmployee(OfficeЕmployee еmployee) {
        employees.add(еmployee);
    }

    public Room(int number, Technics technics) {
        this.number = number;
        this.technics = technics;
    }

    public Technics getTechnics(Technics technics) {
        return technics;
    }

    public void setTechnics(Technics technics) {
        this.technics = technics;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;

    }

    public void info() {
        technics.infoTechnics();
        for (OfficeЕmployee employeesList : employees) {
            System.out.println(employeesList.getSurnameЕmployee() + " " + employeesList.getNameEmployee() +
                    " " + employeesList.getPatronymicЕmployee());

        }
        System.out.println();
    }
}