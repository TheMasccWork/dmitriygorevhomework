package main.java.Office;

import java.util.ArrayList;
import java.util.List;

public class Office {

    private List<Room> rooms = new ArrayList();

    public void addRooms(Room room) {
        rooms.add(room);
    }

    void infoOffice() {

        System.out.println("В офисе " + rooms.size() + " кабинета");
        System.out.println();
        for (Room rooms : rooms) {
            System.out.println("Кабинет " + rooms.getNumber());
            rooms.info();
        }
    }
}


