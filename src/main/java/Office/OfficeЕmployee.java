package main.java.Office;

public class OfficeЕmployee {
    private String nameEmployee;
    private String surnameЕmployee;
    private String patronymicЕmployee;

    OfficeЕmployee(String nameEmployee, String surnameЕmployee, String patronymicЕmployee) {
        this.nameEmployee = nameEmployee;
        this.surnameЕmployee = surnameЕmployee;
        this.patronymicЕmployee = patronymicЕmployee;

    }

    public String getNameEmployee() {
        return nameEmployee;
    }

    public String getSurnameЕmployee() {
        return surnameЕmployee;
    }

    public String getPatronymicЕmployee() {
        return patronymicЕmployee;
    }


}