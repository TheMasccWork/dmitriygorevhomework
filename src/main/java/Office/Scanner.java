package main.java.Office;

public class Scanner extends Technics {

    Scanner(int numberTechnic, boolean enabled) {
        super(numberTechnic, enabled);
    }

    @Override
    public void scan() {
        if (enabled) {
            System.out.println("Работает на сканере с номером " + numberTechnic);
            System.out.println();
        } else {
            System.out.println("Забыл включить сканер с номером " + numberTechnic);
            System.out.println();
        }
    }

    @Override
    public void print() {
    }

    @Override
    public void copy() {
    }

    public void infoTechnics() {
        String x;
        if (getEnabled()) {
            x = "включен";
        } else x = "выключен";
        System.out.println("Сканер номер " + getNumberTechnic() + " " + x);
    }
}
