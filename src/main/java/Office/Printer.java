package main.java.Office;

 public class Printer extends Technics {

     Printer(int numberTechnic, boolean enabled) {
         super(numberTechnic, enabled);
     }

     @Override
     public void print() {
         if (enabled) {
             System.out.println("Работает на принтере с номером " + numberTechnic);
             System.out.println();
         } else {
             System.out.println("Забыл включить принтер с номером " + numberTechnic);
             System.out.println();
         }
     }

     @Override
     public void scan() {
     }

     @Override
     public void copy() {
     }

     public void infoTechnics() {
         String x;
         if (getEnabled()) {
             x = "включен";
         } else x = "выключен";
         System.out.println("Принтер номер " + getNumberTechnic() + " " + x);
     }
 }
