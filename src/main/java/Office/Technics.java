package main.java.Office;

public abstract class Technics {

    protected int numberTechnic;
    protected boolean enabled;

    public Technics(int numberTechnic, boolean enabled) {
        this.numberTechnic = numberTechnic;
        this.enabled = enabled;
    }

    int getNumberTechnic() {
        return numberTechnic;
    }

    public int setNumberTechnic(int numberTechnic) {
        this.numberTechnic = numberTechnic;
        return numberTechnic;
    }

    public boolean getEnabled() {
        return enabled;
    }

    public boolean setEnabled(boolean enabled) {
        this.enabled = enabled;
        return enabled;
    }

    public abstract void scan();

    public abstract void print();

    public abstract void copy();

    public abstract void infoTechnics();



}

