package main.java.Office;

public class Combain extends Technics {

    Combain(int numberTechnic, boolean enabled) {
        super(numberTechnic, enabled);
    }

    @Override
    public void print() {
        if (enabled) {
            System.out.println("Может печатать на комбайне с номером " + numberTechnic);
        } else {
            System.out.println("Забыл включить комбайн с номером " + numberTechnic);
        }
    }

    @Override
    public void scan() {
        if (enabled) {
            System.out.println("Может сканировать на комбайне с номером " + numberTechnic);
        } else {
            System.out.println("Забыл включить комбайн с номером " + numberTechnic);
        }
    }

    @Override
    public void copy() {
        if (enabled) {
            System.out.println("Может копировать на комбайне с номером " + numberTechnic);
            System.out.println();
        } else {
            System.out.println("Забыл включить комбайн с номером " + numberTechnic);
        }
    }

    public void infoTechnics() {
        String x;
        if (getEnabled()) {
            x = "включен";
        } else x = "выключен";
        System.out.println("Комбайн номер " + getNumberTechnic() + " " + x);
    }

}

