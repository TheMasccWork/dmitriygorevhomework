package main.java.lessonTwo;

import java.util.Scanner;

import static java.lang.Math.sqrt;

public class LessonOneExTwo {
    public static void main(String[] args) {
        System.out.println("Для решения квадратного уравнения, введите a, b, c");
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextInt();
        double b = scanner.nextInt();
        double c = scanner.nextInt();
        double d;
        double x;
        double x1;
        double x2;
        d = (b * b - 4 * a * c);
        if (d < 0) {
            System.out.println("Уравнение не имеет решения");
        } else if (d == 0) {
            x = -b / 2 * a;
            System.out.println("Ответ х=" + x);
        } else {
            x1 = (-b + sqrt(d)) / 2 * a;
            x2 = (-b - sqrt(d)) / 2 * a;
            System.out.println("Ответ x1=" + x1 + " x2=" + x2);
        }


    }

}

