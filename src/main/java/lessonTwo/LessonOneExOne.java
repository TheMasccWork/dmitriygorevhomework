package main.java.lessonTwo;

import java.util.Scanner;

public class LessonOneExOne {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int i = scanner.nextInt();
        int b = i%2;
        if ( b==0) {
            System.out.println(i + " четное ");
        } else {
            System.out.println(i + " не четное ");
        }
    }
}