package main.java.Multithreading;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class MultithreadingTwo {
//    Создать массив из 20 элементов в диапазоне значений от -15 до 14 включительно.
//    Определить количество элементов по модулю больших, чем максимальный.
//    Заполнить массив случайными положительными и отрицательными целыми числами.
//    Вывести его на экран. Удалить из массива все отрицательные элементы и снова вывести.

    public static void main(String[] args) {

        List<Integer> list = new ArrayList<>();
        int[] array = new int[20];
        for (int i = 0; i < array.length; i++) {

            array[i] = ((int) (Math.random() * 30) - 15);
            list.add(array[i]);
        }
        System.out.println(list);

        Integer max = list.stream().reduce(Integer::max).orElse(0);
            List<Integer> sql = list.stream().filter(n -> Math.abs(n) > max).collect(Collectors.toList());
            System.out.println("Максимальные числа по модулю " + sql);
            List<Integer> number = list.stream().filter(n -> n >= 0).collect(Collectors.toList());
            System.out.println("Положительные числа массива" + number);
        }


}

