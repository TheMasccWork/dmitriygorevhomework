package main.java.Multithreading;

import java.util.Arrays;
import java.util.List;

public class MultithreadingOne {

//    В массиве найти максимальный отрицательный элемент. Вывести на экран его значение и позицию в массиве.

    public static void main(String[] args) {

        List<Integer> list = Arrays.asList(1, 20, 3, -4, 37, 0, -10, 62);

        Integer min = list.stream().reduce(Integer::min).orElse(0);

        System.out.println("Минимальное значение " + min);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) == min) {
                System.out.println("Позиция в массиве " + i);
            }
        }
    }

}





