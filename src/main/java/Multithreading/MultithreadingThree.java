package main.java.Multithreading;

public class MultithreadingThree {
    public static void main(String[] args) {
        Store store = new Store();
        Produser produser = new Produser(store);
        Consumer consumer = new Consumer(store);
        new Thread(produser).start();
        new Thread(consumer).start();

    }
}

class Store {
    private int product = 0;

    public synchronized void get() {
        while (product < 6) {
            try {
                wait();
            } catch (InterruptedException ignored) {
            }
        }
        product--;
        System.out.println("Покупатель купил 1 товар ");
        System.out.println("Общее количество товаров: " + product);
        notify();
    }

    public synchronized void put() {
        while (product >= 6) {
            try {
                wait();
            } catch (InterruptedException ignored) {

            }
        }
        product++;
        System.out.println("Производитель добавил 1 товар на витрину ");
        System.out.println("Общее количество товаров: " + product);
        notify();
    }
}

class Produser implements Runnable {

    Store store;

    Produser(Store store) {
        this.store = store;
    }

    public void run() {
        for (int i = 1; i < 10; i++) {
            store.put();
        }
    }

}

class Consumer implements Runnable {

    Store store;

    Consumer(Store store) {
        this.store = store;
    }

    public void run() {
        for (int i = 1; i < 10; i++) {
            store.get();
        }
    }
}